// Matthew Marcellinus
// CS410P
// Spring 2022
// Computer, Sound, & Music
// Course Project
#include "rhythm_extender_original.h"

void TimeStretcherFD::renew(const double& detail, vector<double> mag, vector<double> before, vector<double> after, vector<double>& fusion) {
    for(int repetition_bin=0; repetition_bin<methods/2+1; ++repetition_bin)
        fusion[repetition_bin] = fmod(fusion[repetition_bin]+after[repetition_bin]-before[repetition_bin], 2.0*constant);
}

void TimeStretcherFD::Stretch(const double& detail, vector<Compose*>& store_spec, vector<Compose*>& volume, int &synthesis_compose_shift, bool recalibrate) {
    vector<double> mag, ph;
    Compose *f;
    double sample_ptr = 0.0; //Indicator towards earlier range. And latest immensity must integrated 
    while ((unsigned int)sample_ptr+1<store_spec.size())
    {
        int prev_compose_idx = (int)sample_ptr;
        double prev_compose_weight = 1-(sample_ptr-(int)sample_ptr);
        int next_compose_idx = prev_compose_idx+1;
        double next_compose_weight = 1-prev_compose_weight;

        mag = linear(store_spec[prev_compose_idx]->extent(), store_spec[next_compose_idx]->extent(), prev_compose_weight, next_compose_weight);

        f = new Compose(methods);
        if (volume.size()>0)
            renew(detail, mag, store_spec[prev_compose_idx]->form(), store_spec[next_compose_idx]->form(), ph);
        else //Split earliest setting
        {
            if (recalibrate)
                ph = store_spec[0]->form();
            else
            {
                ph = stash;
                renew(detail, mag, stash, store_spec[0]->form(), ph);
            }
        }
        SynthesizeCompose(mag, ph, f);
        volume.push_back(f);

        sample_ptr += 1.0/detail;
    }

    for(int repetition_bin=0; repetition_bin<methods/2+1; ++repetition_bin)
        stash[repetition_bin] = ph[repetition_bin];
}
