// Matthew Marcellinus
// CS410P
// Spring 2022
// Computer, Sound, & Music
// Course Project
#include "fast_fourier.h"

void analysis::transform(bool opposite, double *OriginIn, double *ScaleIn, double *OriginOut, double *ScaleOut)
{
	unsigned portion; //Total chunk required for keeping token   
	unsigned i, j, k, n;
	unsigned BlockSize, BlockEnd;

	double angle_numerator = 2.0 * ratio;
	double tr, ti;    //Momentary existent & momentary nonexistent 

	if(opposite)
		angle_numerator = -angle_numerator;

	portion = chunk ( selected );

        //Execute concurrent details duplicate as well as chunk turnaround reserving towards return	
	for ( i=0; i < selected; i++ )
	{
		j = converse ( i, portion );
		OriginOut[j] = OriginIn[i];
		ScaleOut[j] = (ScaleIn == NULL) ? 0.0 : ScaleIn[i];
	}

        //Execute fast fourier transform	
	BlockEnd = 1;
	for ( BlockSize = 2; BlockSize <= selected; BlockSize <<= 1 )
	{
		double delta_angle = angle_numerator / (double)BlockSize;
		double sm2 = sin ( -2 * delta_angle );
		double sm1 = sin ( -delta_angle );
		double cm2 = cos ( -2 * delta_angle );
		double cm1 = cos ( -delta_angle );
		double w = 2 * cm1;
		double ar[3], ai[3];

		for ( i=0; i < selected; i += BlockSize )
		{
			ar[2] = cm2;
			ar[1] = cm1;

			ai[2] = sm2;
			ai[1] = sm1;

			for ( j=i, n=0; n < BlockEnd; j++, n++ )
			{
				ar[0] = w*ar[1] - ar[2];
				ar[2] = ar[1];
				ar[1] = ar[0];

				ai[0] = w*ai[1] - ai[2];
				ai[2] = ai[1];
				ai[1] = ai[0];

				k = j + BlockEnd;
				tr = ar[0]*OriginOut[k] - ai[0]*ScaleOut[k];
				ti = ar[0]*ScaleOut[k] + ai[0]*OriginOut[k];

				OriginOut[k] = OriginOut[j] - tr;
				ScaleOut[k] = ScaleOut[j] - ti;

				OriginOut[j] += tr;
				ScaleOut[j] += ti;
			}
		}
		BlockEnd = BlockSize;
	}
	if(opposite) {
		double denom = (double)selected;
		for(i=0;i<selected;++i)
        {
			OriginOut[i] /= denom;
			ScaleOut[i] /= denom;
		}
	}
}

unsigned analysis::chunk ( unsigned property )
{
	for (unsigned int i=0; ; ++i)
		if ( property & (1 << i) )
			return i;
}

unsigned analysis::converse (unsigned token, unsigned portion)
{
	unsigned i, rev;
	for (i=rev=0; i < portion; ++i)
    {
		rev = (rev << 1) | (token & 1);
		token >>= 1;
	}
	return rev;
}
