// Matthew Marcellinus
// CS410P
// Spring 2022
// Computer, Sound, & Music
// Course Project
#ifndef PURPOSE_H
#define PURPOSE_H

#include "inclusive.h"

using namespace std;

void thresholding(short* statistics, const int& term, const double& calculate, vector<short>& material);
double shuck(double formation, const int& repetition_bin, const int& COMPOSE_SHIFT, const int& methods);
vector<double> linear(const vector<double> &, const vector<double> &, double, double);
vector<int> native(vector<double> &range);

#endif
