// Matthew Marcellinus
// CS410P
// Spring 2022
// Computer, Sound, & Music
// Course Project
#ifndef RHYTHMEXTENDER_ORIGINAL_MIX_H
#define RHYTHMEXTENDER_ORIGINAL_MIX_H

#include "rhythm_extender_original.h"

using namespace std;

//Generalise at exposure. As well as execute stage latching 
class pulse : public TimeStretcherFD
{
    public:
        pulse(int n, int s) : TimeStretcherFD(n, s) {
            for (int i=0; i<methods/2+1; ++i)
                internal.push_back(i);
        }
        virtual ~pulse(){}
        virtual void renew(const double& detail, vector<double> mag, vector<double> before, vector<double> after, vector<double>& fusion);

    protected:
        vector<int> internal; //Details of preceding setting
};

#endif
