// Matthew Marcellinus
// CS410P
// Spring 2022
// Computer, Sound, & Music
// Course Project
#include "rhythm_extender_original_mix.h"

void pulse::renew(const double& detail, vector<double> mag, vector<double> before, vector<double> after, vector<double>& fusion) {
    vector<int> local_peaks = native(mag);

    for(int repetition_bin=0; repetition_bin<methods/2+1; ++repetition_bin)
        if (repetition_bin==local_peaks[repetition_bin])
        {
            double formation = after[repetition_bin]-before[internal[repetition_bin]];
            fusion[repetition_bin] = fmod(fusion[repetition_bin]+shuck(formation, repetition_bin, COMPOSE_SHIFT, methods), 2.0*constant);
        }

    for(int repetition_bin=0; repetition_bin<methods/2+1; ++repetition_bin)
        if (repetition_bin!=local_peaks[repetition_bin])
        {
            double formation = after[repetition_bin]-after[local_peaks[repetition_bin]];
            fusion[repetition_bin] = fmod(fusion[local_peaks[repetition_bin]]+formation, 2.0*constant);
        }

    internal = local_peaks;
}
