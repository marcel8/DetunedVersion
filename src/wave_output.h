// Matthew Marcellinus
// CS410P
// Spring 2022
// Computer, Sound, & Music
// Course Project
#ifndef WAVEOUTPUT_H
#define WAVEOUTPUT_H

#include "inclusive.h"
using namespace std;

class encoding
{
    public:
        encoding(){}

        encoding(string &limited)
        {
            direction = limited;
            read();
        }

        ~encoding()
        {
            if (little) delete [] little;
        }

        string route() {return direction;}
        void course(string &current) {direction = current;}

        //Interpret waveform audio 
        bool read();

        //Record waveform audio
        bool save();

        //Give back impart outline from waveform audio
        string abstract();

        short* little;
        int information; //read() is deconstructed
                         //char->short. Real content are char
                         //diverged alongside 2
        int specific;

    private:
        string	direction;
        int 	piece;
        int	square;
        short 	scheme;
        short 	technique;
        int   	digit;
        short 	section;
        short 	item;
};

#endif
