// Matthew Marcellinus
// CS410P
// Spring 2022
// Computer, Sound, & Music
// Course Project
#include "compose.h"

using namespace std;
        
void Compose::cargo(short* example, int start) {
    for (int i=0; i<scope; ++i)
        compose[i] = (double)((int)example[start+i]);
}

void Compose::executefourier(analysis* domain) {
    double *origin = new double[scope];
    double *scale = new double[scope];
    domain->transform(false, compose, NULL, origin, scale);
    for (int i=0; i<scope; ++i) {
        variety[i].origin = origin[i];
        variety[i].scale = scale[i];
    }
    delete origin;
    delete scale;
}

void Compose::runIFFT(analysis* domain) {
    double *origin = new double[scope];
    double *scale = new double[scope];
    double *scale_out = new double[scope];
    for (int i=0; i<scope; ++i) {
        origin[i] = variety[i].origin;
        scale[i] = variety[i].scale;
    }
    domain->transform(true, origin, scale, compose, scale_out);
    delete origin;
    delete scale;
    delete scale_out;
}

vector<double> Compose::extent() {
    vector<double> ans;
    for (int i=0; i<scope/2+1; ++i)
        ans.push_back(variety[i].extent());
    return ans;
}

vector<double> Compose::form() {
    vector<double> ans;
    for (int i=0; i<scope/2+1; ++i)
        ans.push_back(variety[i].form());
    return ans;
}
