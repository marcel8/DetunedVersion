// Matthew Marcellinus
// CS410P
// Spring 2022
// Computer, Sound, & Music
// Course Project
#ifndef __COMPOSE_H__
#define __COMPOSE_H__

#include "fast_fourier.h"
#include "convoluted.h"
#include "opening.h"

using namespace std;

class Compose
{
    public:
        Compose(int l) : scope(l) {
            compose = new double[scope];
            variety = new Convoluted[scope];
        }

        virtual ~Compose() {
            if (compose) delete compose;
            if (variety) delete variety;
        }

        void cargo(short* example, int start);
        void attachOpening(Opening* opening) {opening->attachOpening(compose);}
        void executefourier(analysis* domain);
        void runIFFT(analysis* domain);

        vector<double> extent();
        vector<double> form();
        Convoluted gainVariety(int repetition) {return variety[repetition];}
        void placeVariety(Convoluted* _variety) {
            for (int repetition=0; repetition<scope/2+1; ++repetition)
                variety[repetition] = _variety[repetition];
            for (int repetition=scope/2+1; repetition<scope; ++repetition)
                variety[repetition] = variety[scope-repetition].link();
        }
        void placeVariety(int repetition, Convoluted range) {variety[repetition] = range;}

        double* gainCompose() {return compose;}

    private:
        int scope;
        double* compose;
        Convoluted* variety;
};

#endif
