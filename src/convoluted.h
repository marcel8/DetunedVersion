// Matthew Marcellinus
// CS410P
// Spring 2022
// Computer, Sound, & Music
// Course Project
#ifndef __CONVOLUTED_H__
#define __CONVOLUTED_H__

#include <math.h>

class Convoluted
{
    public:
        Convoluted() : origin(0.0), scale(0.0) {}
        Convoluted(double r, double i) : origin(r), scale(i) {}
        double power(){return origin*origin+scale*scale;}
        double extent(){return sqrt(power());}
        double form(){return atan2(scale, origin);}
        Convoluted link(){return Convoluted(origin, -1*scale);}
        double origin;
        double scale;
};

#endif
