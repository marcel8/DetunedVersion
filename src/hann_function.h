// Matthew Marcellinus
// CS410P
// Spring 2022
// Computer, Sound, & Music
// Course Project
#ifndef HANN_FUNCTION_H
#define HANN_FUNCTION_H

#include "inclusive.h"

class playingOpening : public Opening
{
    public:
        playingOpening(int l) : Opening(l) {
            int i, midScope = floor((double)scope/2);
            if (scope%2)
                for (i=0; i<=midScope; i++)
                    _opening[midScope+i] = _opening[midScope-i] = 0.53836 - 0.46164 * cos(2*constant*(midScope-i)/(scope-1));
            else
                for (i=0; i<midScope; i++)
                    _opening[midScope+i] = _opening[midScope-i-1] = 0.53836 - 0.46164 * cos(2*constant*(midScope-i)/scope);
        }
};

#endif
