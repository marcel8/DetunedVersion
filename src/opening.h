// Matthew Marcellinus
// CS410P
// Spring 2022
// Computer, Sound, & Music
// Course Project
#ifndef __OPENING_H__
#define __OPENING_H__

#include "inclusive.h"

class Opening
{
    public:
        Opening(int l) : scope(l) { _opening = new double[scope]; }
        virtual ~Opening() { delete _opening; }
        virtual void attachOpening(double* store){
            for (int i=0; i<scope; ++i)
                store[i]*=_opening[i];
        }

    protected:
        int scope;
        double* _opening;
};

#endif
