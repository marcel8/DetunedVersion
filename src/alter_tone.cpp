// Matthew Marcellinus
// CS410P
// Spring 2022
// Computer, Sound, & Music
// Course Project
#include "alter_tone.h"

void key::renew(const double& part, vector<double>& mag, vector<double> before, vector<double> after, vector<double>& fusion) {
    for(int i=0;i<methods/2+1;i++) {
        beat[i] = floor(i*part);
        remaining[i] = i*part-beat[i];
        fusion[i] = fmod(fusion[i]+part*shuck(after[i]-before[i], i, COMPOSE_SHIFT, methods), 2.0*constant);
    }
}

void key::SynthesizeCompose(vector<double>& mag, vector<double>& ph, Compose *f) {
    vector<Convoluted> synth_spec(methods/2+1, Convoluted(0.0, 0.0));
    double energy=0, new_energy=0; //Conducive to power maintenance 

   
    //Insert convoluted range with reiteration center line 
    for(int i=0;i<methods/2+1;i++) {
        energy += mag[i]*mag[i];
        if (beat[i]>=0 && beat[i]<methods/2+1) {
            synth_spec[beat[i]].origin += (1-remaining[i])*mag[i]*cos(ph[i]);
            synth_spec[beat[i]].scale += (1-remaining[i])*mag[i]*sin(ph[i]);
        }
        if (beat[i]+1>=0 && beat[i]+1<methods/2+1) {
            synth_spec[beat[i]+1].origin += remaining[i]*mag[i]*cos(ph[i]);
            synth_spec[beat[i]+1].scale += remaining[i]*mag[i]*sin(ph[i]);
        }
    }

    //Power maintenance 
    for (int i=0; i<methods/2+1; i++)
        new_energy += synth_spec[i].power();
    double amplitude_norm = sqrt(energy/new_energy);
    for (int i=0; i<methods/2+1; i++) {
        synth_spec[i].origin *= amplitude_norm;
        synth_spec[i].scale *= amplitude_norm;
    }

    //Result 
    f->placeVariety(&synth_spec[0]);
}

void key::Shift(const double& part, vector<Compose*>& store_spec, vector<Compose*>& volume, bool recalibrate)
{
    vector<int> subband;
    vector<double> mag, ph;
    Compose *f;

    for (unsigned int sample_idx=0; sample_idx<store_spec.size(); ++sample_idx) {
        mag = store_spec[sample_idx]->extent();
        if (sample_idx)
            renew(part, mag, store_spec[sample_idx-1]->form(), store_spec[sample_idx]->form(), ph);
        else {
            if (recalibrate) {
                ph = store_spec[0]->form();
                for (int i=0; i<methods/2+1; i++)
                    beat[i] = (int)(i*part);
            }
            else {
                ph = stash;
                renew(part, mag, stash, store_spec[0]->form(), ph);
            }
        }
        f = new Compose(methods);
        SynthesizeCompose(mag, ph, f);
        volume.push_back(f);
    }
    stash = ph;
}
