// Matthew Marcellinus
// CS410P
// Spring 2022
// Computer, Sound, & Music
// Course Project
#ifndef __KEY_H__
#define __KEY_H__

#include "handle.h"
#include "compose.h"

using namespace std;

//Denoising convoluted range with reiteration center line during looking after stages
class key
{
    public:
        key(int n, int s) : methods(n), COMPOSE_SHIFT(s) {
            stash = vector<double>(n/2+1, 0.0);
            remaining = vector<double>(n/2+1, 0.0);
            beat = vector<int>(n/2+1, 0);
        }
        virtual ~key() {}
        virtual void renew(const double& part, vector<double>& mag, vector<double> before, vector<double> after, vector<double>& fusion);
        virtual void SynthesizeCompose(vector<double>& mag, vector<double>& ph, Compose *f);
        virtual void Shift(const double& part, vector<Compose*>& store_spec, vector<Compose*>& volume, bool recalibrate);

    protected:
        int methods;
        int COMPOSE_SHIFT;
        vector<double> stash; //Previous stage range within former switch invocation 
        vector<int> beat;
        vector<double> remaining; //Conducive to insert
};

#endif
