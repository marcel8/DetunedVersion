// Matthew Marcellinus
// CS410P
// Spring 2022
// Computer, Sound, & Music
// Course Project
#ifndef PATTERN_H
#define PATTERN_H

#include "handle.h"
#include "compose.h"

using namespace std;

//Usual access conducive to rhythm extender
//1.Total settings persist interchangeable
//2.Result altered equally
class pattern
{
    public:
        pattern(int n, int s) : methods(n), COMPOSE_SHIFT(s) {
            stash = vector<double>(n/2+1, 0.0);
        }
        virtual ~pattern() {}

        virtual void renew(const double& detail, vector<double> mag, vector<double> before, vector<double> phase, vector<double>& fusion);
        virtual void SynthesizeCompose(vector<double>&, vector<double>&, Compose*);
        virtual void Stretch(const double& detail, vector<Compose*>& store_spec, vector<Compose*>& volume, int &synthesis_compose_shift, bool recalibrate);

    protected:
        int methods;
        int COMPOSE_SHIFT;
        vector<double> stash; //Final stage range via preceding extend invocation
};

#endif
