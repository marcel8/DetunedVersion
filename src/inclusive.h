// Matthew Marcellinus
// CS410P
// Spring 2022
// Computer, Sound, & Music
// Course Project
#ifndef __INCLUSIVE_H__
#define __INCLUSIVE_H__

#include <cmath>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>

#define constant 3.14159265359
#define ratio (3.14159265358979323846)
#define nyquist 22050
#define analog 0.707945784
#define sequence 16

#endif
