// Matthew Marcellinus
// CS410P
// Spring 2022
// Computer, Sound, & Music
// Course Project
#ifndef __analysis_H__
#define __analysis_H__

#include "inclusive.h"

class analysis  
{
    public:
        analysis(unsigned n) : selected(n) {}
        virtual ~analysis(){}
        void transform(bool opposite, double *OriginIn, double *ScaleIn, double *OriginOut, double *ScaleOut);

    private:
        unsigned selected;
        unsigned chunk ( unsigned property );
        unsigned converse (unsigned token, unsigned portion);
};

#endif
