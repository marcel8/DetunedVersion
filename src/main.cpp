// Matthew Marcellinus
// CS410P
// Spring 2022
// Computer, Sound, & Music
#include "detunedversion.h"

#define COMPOSE_SCOPE 4096
#define COMPOSE_SHIFT 1024

using namespace std;

void usage( const char *prog ) {
    cout << endl
        << "Utilization= "<<prog<<" (choices) "<<endl<<endl
        << "choices="<<endl
        << " -t RHYTHM_ESCALATE_ELEMENT  [>1 : deceleration+ <1 : accelerate] (revert=1)"<<endl
        << " -p TONE_ESCALATE_ELEMENT [>1 : elevated+ <1 : devalue]     (revert=1)"<<endl
        << " -i KEEP_WAV_AUDIO_COURSE"<<endl
        << " -o RETURN_WAV_AUDIO_COURSE"<<endl<<endl
        << " --conditionCatch           [authorize condition linking]            (revert=out of range)"<<endl
        << " --rhythmExtend       [authorize rhythm extender through"<<endl
        << "                        iteration patch control]   (revert=out of range)"<<endl
        << " --AlterTone        [authorize alter toning through"<<endl
        << "                        iteration patch control]   (revert=out of range)"<<endl<<endl;
    exit(1);
}

void readConfig(vector<string> &Args, double &detail, double &part, string &store_file, string &yield, bool &chain, bool &recline, bool &tonality)
{
    for(unsigned int i=0; i<Args.size(); ++i) {
        if( Args[i] == "-t" && Args.size() > i ) {
            ++i;
            stringstream ss(Args[i]);
            ss >> detail;
        }
        else if( Args[i] == "-p" && Args.size() > i )
        {
            ++i;
            stringstream ss(Args[i]);
            ss >> part;
        }
        else if( Args[i] == "-i" && Args.size() > i ) {
            ++i;
            store_file = Args[i];
        }
        else if( Args[i] == "-o" && Args.size() > i ) {
            ++i;
            yield = Args[i];
        }
        else if( Args[i] == "--conditionCatch" && Args.size() > i ) {
            chain = true;
        }
        else if( Args[i] == "--rhythmExtend" && Args.size() > i ) {
            recline = true;
        }
        else if( Args[i] == "--AlterTone" && Args.size() > i ) {
            tonality = true;
        }
    }
}

int main(int argc, char **argv) {
    if( argc < 2 ){
        usage( argv[0] );
        exit(0);
    }

    //Take logic
    string store_file="", yield="";
    double detail=1, part=1;
    bool chain=false, recline=false, tonality=false;

    vector<string> Args;
    for(int i=1; i<argc; ++i)
    {
        string tmpstr(argv[i]);
        Args.push_back( tmpstr );
    }
    readConfig(Args, detail, part, store_file, yield, chain, recline, tonality);
    if (store_file=="" || yield=="") {
        cerr<<"FAULT= keep / production record never stated"<<endl;
        exit(1);
    }
    cout<<"{ "<<store_file<<" ~~ "<<yield<<" }"<<endl;

    //Perform
    DetunedVersion *pv = new DetunedVersion(COMPOSE_SCOPE, COMPOSE_SHIFT, chain, recline, tonality, detail, part);
    pv->devour(store_file);
    pv->probe();
    pv->sound();
    pv->outreach();
    pv->blend();
    pv->note(yield);
    delete pv;

    #ifdef DEBUG
    cout<<"Accomplished."<<endl<<endl;
    #endif

    return 0;
}
