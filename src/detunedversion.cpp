// Matthew Marcellinus
// CS410P
// Spring 2022
// Computer, Sound, & Music
#include "detunedversion.h"

using namespace std;

DetunedVersion::DetunedVersion(int compose_scope, int compose_shift, bool chain, bool recline, bool tonality, double element, double feature)
{
    #ifdef DEBUG
    cout<<"Carve up framework as well as tasks"<<endl;
    #endif
    analysis_compose_shift = synthesis_compose_shift = compose_shift;
    methods = compose_scope;
    opening = new playingOpening(methods);
    domain = new analysis(methods);
    detail = element;
    part = feature;
    if (chain)
    {
        if (recline)
            ts = new pulse(methods, analysis_compose_shift);
        else
            ts = new flow(methods, analysis_compose_shift);
    }
    else
    {
        if (recline)
            ts = new TimeStretcherFD(methods, analysis_compose_shift);
        else
            ts = new pattern(methods, analysis_compose_shift);
    }
    if (tonality)
        ps = new key(methods, analysis_compose_shift);
    else
        ps = NULL;
}
    
DetunedVersion::~DetunedVersion()
{
    delete ts;
    if (ps) delete ps;
    delete opening;
    delete domain;
    if (store_wav) delete store_wav;
    if (produce) delete produce;
    for (vector<Compose*>::iterator f=spectrogram.begin(); f!=spectrogram.end(); ++f) delete (*f);
}

void DetunedVersion::devour(string store_file)
{
    #ifdef DEBUG
    cout<<"Interpret "<<store_file<<endl;
    #endif
    store_wav = new encoding(store_file);
    sampling_rate = store_wav->specific;
    expanse = store_wav->information/2*detail;
    #ifdef DEBUG
    cout<<store_wav->abstract()<<endl;
    #endif
}

void DetunedVersion::probe()
{
    vector<short> statistics;
    if (ps==NULL && part!=1) {
        thresholding(store_wav->little, store_wav->information/2, part, statistics); //When part==1 (It means identical with duplicating information) 
        detail*=part;
        part = 1;
    }
    else
        thresholding(store_wav->little, store_wav->information/2, 1, statistics);

    int num_compose = (statistics.size()-methods)/analysis_compose_shift+1;
    for (int compose_idx=0; compose_idx<num_compose; ++compose_idx) {
        Compose *f = new Compose(methods);
        f->cargo(&(statistics[0]), compose_idx*analysis_compose_shift);
        f->attachOpening(opening);
        f->executefourier(domain);
        spectrogram.push_back(f);
    }
}

void DetunedVersion::outreach()
{
    if (ts!=NULL && detail!=1) {
        #ifdef DEBUG
        cout<<"Rhythm extender"<<endl;
        #endif
        vector<Compose*> new_spectrogram;
        ts->Stretch(detail, spectrogram, new_spectrogram, synthesis_compose_shift, true);
        for (vector<Compose*>::iterator f=spectrogram.begin(); f!=spectrogram.end(); ++f) delete (*f);
        spectrogram = new_spectrogram;
    }
}

void DetunedVersion::sound()
{
    if (ps!=NULL && part!=1) {
        #ifdef DEBUG
        cout<<"Alter toning"<<endl;
        #endif
        vector<Compose*> new_spectrogram;
        ps->Shift(part, spectrogram, new_spectrogram, true);
        for (vector<Compose*>::iterator f=spectrogram.begin(); f!=spectrogram.end(); ++f) delete (*f);
        spectrogram = new_spectrogram;
    }
}

void DetunedVersion::blend()
{
    #ifdef DEBUG
    cout<<"Combination"<<endl;
    #endif
    vector<double> square_opening(methods, 1.0); //Conducive to divisor at blend 
    opening->attachOpening(&square_opening[0]);
    opening->attachOpening(&square_opening[0]);

    cue = new short[expanse];
    for (unsigned int i=0; i<expanse; ++i) cue[i]=0;

    vector<double> synth_normalize_coeff(expanse, 0.0);
    for (unsigned int compose_idx=0; compose_idx<spectrogram.size(); ++compose_idx) {
        spectrogram[compose_idx]->runIFFT(domain);
        spectrogram[compose_idx]->attachOpening(opening);
        double *compose = spectrogram[compose_idx]->gainCompose();
        for (unsigned int sample_idx=0; sample_idx<methods && compose_idx*synthesis_compose_shift+sample_idx<expanse; ++sample_idx) {
            cue[compose_idx*synthesis_compose_shift+sample_idx]+=compose[sample_idx];
            synth_normalize_coeff[compose_idx*synthesis_compose_shift+sample_idx]+=square_opening[sample_idx];
        }
    }
    for (unsigned int i=0; i<expanse; ++i)
        cue[i]/=synth_normalize_coeff[i];
}

void DetunedVersion::note(string yield)
{
    #ifdef DEBUG
    cout<<"Note "<<yield<<endl;
    #endif
    produce = new encoding(*store_wav);
    produce->course(yield);
    produce->information = expanse*2;
    produce->little = cue;
    produce->save();
}
