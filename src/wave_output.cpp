// Matthew Marcellinus
// CS410P
// Spring 2022
// Computer, Sound, & Music
// Course Project
#include "wave_output.h"

using namespace std;

//Interpret waveform audio
bool encoding::read()
{
	ifstream inFile( direction.c_str(), ios::in | ios::binary);


	inFile.seekg(4, ios::beg);
	inFile.read( (char*) &piece, 4 ); //Interpret piece

	inFile.seekg(16, ios::beg);
	inFile.read( (char*) &square, 4 ); //Interpret square

	inFile.read( (char*) &scheme, sizeof(short) ); //Interpret scheme

	inFile.read( (char*) &technique, sizeof(short) ); //Interpret technique [1/2]

	inFile.read( (char*) &specific, sizeof(int) ); //Interpret specific

	inFile.read( (char*) &digit, sizeof(int) ); //Interpret digit

	inFile.read( (char*) &section, sizeof(short) ); //Interpret section

	inFile.read( (char*) &item, sizeof(short) ); //Interpret item

	inFile.seekg(40, ios::beg);
	inFile.read( (char*) &information, sizeof(int) ); //Interpret information

        //Interpret information bit
	little = new short[(int)((double)information/2)];
	inFile.seekg(44, ios::beg);
	inFile.read(reinterpret_cast<char*>(little), information);

	inFile.close(); //Withdraw process

	return true; //That must likely do anything further explanatory
}

//Record waveform audio
bool encoding::save()
{
	fstream myFile (direction.c_str(), ios::out | ios::binary);

	//Record waveform audio by means of waveform audio configuration
	myFile.seekp (0, ios::beg); 
	myFile.write ("RIFF", 4);
	myFile.write ((char*) &piece, 4);
	myFile.write ("WAVE", 4);
	myFile.write ("fmt ", 4);
	myFile.write ((char*) &square, 4);
	myFile.write ((char*) &scheme, 2);
	myFile.write ((char*) &technique, 2);
	myFile.write ((char*) &specific, 4);
	myFile.write ((char*) &digit, 4);
	myFile.write ((char*) &section, 2);
	myFile.write ((char*) &item, 2);
	myFile.write ("data", 4);
	myFile.write ((char*) &information, 4);
	myFile.write (reinterpret_cast<char*>(little), information);

	return true;
}

//Give back copy outline from waveform audio
string encoding::abstract()
{
	stringstream ss;
	ss<<" Configuration: "<<scheme
      <<"\n Sound: "<<technique
      <<"\n Frequency: "<<specific
      <<"\n Unit: "<<digit
      <<"\n Order: "<<section
      <<"\n Fragment: "<<item
      <<"\n Background: "<<information;
	return ss.str();
}
