// Matthew Marcellinus
// CS410P
// Spring 2022
// Computer, Sound, & Music
// Course Project
#include "handle.h"

void thresholding(short* statistics, const int& term, const double& calculate, vector<short>& material)
{
    double idx = 0;
    while (idx<term)
    {
        material.push_back(statistics[(int)idx]);
        idx += calculate;
    }
}

double shuck(double formation, const int& repetition_bin, const int& COMPOSE_SHIFT, const int& methods) {
    formation -= 2*constant*repetition_bin*COMPOSE_SHIFT/methods;
    formation = fmod(formation, 2.0*constant);
    return formation + 2*constant*repetition_bin*COMPOSE_SHIFT/methods;
}

vector<double> linear(const vector<double> &v1, const vector<double> &v2, double w1, double w2) {
    if (w2==0) return v1;
    if (w1==0) return v2;
    vector<double> ans;
    if (v1.size()!=v2.size()) return ans;
    for (unsigned int i=0; i<v1.size(); ++i)
        ans.push_back(v1[i]*w1+v2[i]*w2);
    return ans;
}

//Insert=
//[0,1,2,1,0,3,6,5,0]
//Return=
//[2,2,2,2,7,7,7,7,7]
//Interpretation=
//Top= Bigger compared to foregoing 2 as well as retinue 2 components
//Channel= Base connecting 2 tops
vector<int> native(vector<double>& range) {
    vector<int> peak_idx;
    int peak_ptr = 0;
    int valley_ptr = 0;
    peak_idx.push_back(0);
    peak_idx.push_back(0);
    for (unsigned int i=2; i<range.size()-2; ++i) {
        if (range[i] > range[i-1] && range[i] > range[i-2] && range[i] > range[i+1] && range[i] > range[i+2]) {
            //Top= Initialize of channel directed toward latest location
            for (unsigned int j=valley_ptr; j<i; ++j)
                peak_idx[j] = i;
            peak_ptr = i;
            valley_ptr = i;
        }
        else //Or else= Renovate channel on condition that obligatory
            if (range[i]<range[valley_ptr])
                valley_ptr = i;
        peak_idx.push_back(peak_ptr);
    }
    peak_idx.push_back(peak_ptr);
    peak_idx.push_back(peak_ptr);
    return peak_idx;
}
