// Matthew Marcellinus
// CS410P
// Spring 2022
// Computer, Sound, & Music
// Course Project
#include "rhythm_extender.h"

void pattern::renew(const double& detail, vector<double> mag, vector<double> before, vector<double> after, vector<double>& fusion) {
    for(int repetition_bin=0; repetition_bin<methods/2+1; ++repetition_bin)
        fusion[repetition_bin] = fmod(fusion[repetition_bin]+(after[repetition_bin]-before[repetition_bin])*detail, 2.0*constant);
}

void pattern::SynthesizeCompose(vector<double>& mag, vector<double>& ph, Compose* f){
    for(int repetition_bin=0; repetition_bin<methods/2+1; ++repetition_bin)
        f->placeVariety(repetition_bin, Convoluted(mag[repetition_bin]*cos(ph[repetition_bin]), mag[repetition_bin]*sin(ph[repetition_bin])));
    for(int repetition_bin=methods/2+1; repetition_bin<methods; ++repetition_bin)
        f->placeVariety(repetition_bin, f->gainVariety(methods-repetition_bin).link());
}

void pattern::Stretch(const double& detail, vector<Compose*>& store_spec, vector<Compose*>& volume, int &synthesis_compose_shift, bool recalibrate) {
    vector<double> mag, ph;
    Compose *f;
    for (unsigned compose_idx=0; compose_idx<store_spec.size(); ++compose_idx) {
        mag = store_spec[compose_idx]->extent();
        f = new Compose(methods);
        if (compose_idx > 0)
            renew(detail, mag, store_spec[compose_idx-1]->form(), store_spec[compose_idx]->form(), ph);
        else //Split earliest setting
        {
            if (recalibrate)
                ph = store_spec[0]->form();
            else
            {
                ph = stash;
                renew(detail, mag, stash, store_spec[0]->form(), ph);
            }
        }
        SynthesizeCompose(mag, ph, f);
        volume.push_back(f);
    }

    for(int repetition_bin=0; repetition_bin<methods/2+1; ++repetition_bin)
        stash[repetition_bin] = ph[repetition_bin];

    synthesis_compose_shift*=detail;
}
