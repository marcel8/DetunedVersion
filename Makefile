CXX=g++
CXXFLAGS=-O3 -Wall

SRC=src
OBJ=obj
BIN=bin

SOURCES=main.cpp \
    handle.cpp \
    rhythm_extender.cpp \
    rhythm_extender_mix.cpp \
    rhythm_extender_original.cpp \
    rhythm_extender_original_mix.cpp \
    fast_fourier.cpp \
    compose.cpp \
    wave_output.cpp \
    alter_tone.cpp \
    detunedversion.cpp

OBJECTS:=$(addprefix $(OBJ)/, $(addsuffix .o,$(basename $(SOURCES))))

.PHONY: dir clean

all: dir program

debug: CXXFLAGS += -DDEBUG -g
debug: all

dir:
	mkdir -p $(BIN) $(OBJ)

program: $(OBJECTS)
	$(CXX) $(CXXFLAGS) -o $(BIN)/DetunedVersion $^

$(OBJ)/%.o: $(SRC)/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

clean:
	rm -rf $(BIN) $(OBJ)
