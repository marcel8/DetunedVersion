mkdir -p result/

# 1/2 PACE
../bin/DetunedVersion -t 2.0 -i gc.wav -o result/Sound_1.wav
# 2X PACE
../bin/DetunedVersion -t 0.5 -i gc.wav -o result/Sound_2.wav
# 1/2 PACE + Condition Fastened
../bin/DetunedVersion -t 2.0 -i gc.wav -o result/Sound_3.wav --conditionCatch
# 2X PACE + Condition Fastened
../bin/DetunedVersion -t 0.5 -i gc.wav -o result/Sound_4.wav --conditionCatch
# 1/2 PACE + Play Disgression
../bin/DetunedVersion -t 2.0 -i gc.wav -o result/Sound_5.wav --rhythmExtend
# 2X PACE + Play Disgression
../bin/DetunedVersion -t 0.5 -i gc.wav -o result/Sound_6.wav --rhythmExtend
# 1/2 PACE + Play Disgression
../bin/DetunedVersion -t 2.0 -i gc.wav -o result/Sound_7.wav --rhythmExtend --conditionCatch
# 2X PACE + Play Disgression
../bin/DetunedVersion -t 0.5 -i gc.wav -o result/Sound_8.wav --rhythmExtend --conditionCatch
# 1 Entire Pace Elevated
../bin/DetunedVersion -p 1.125 -i gc.wav -o result/Sound_9.wav
# 1 Entire Pace Devalue
../bin/DetunedVersion -p 0.889 -i gc.wav -o result/Sound_10.wav
# 1 Entire Pace Elevated
../bin/DetunedVersion -p 1.125 -i gc.wav -o result/Sound_11.wav --AlterTone
# 1 Entire Pace Devalue
../bin/DetunedVersion -p 0.889 -i gc.wav -o result/Sound_12.wav --AlterTone
